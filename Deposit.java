public class Deposit extends BankAccount{
    private int balance;

    public void setBalance(int amount) {
       balance= balance + amount;
       if(balance< 0){
           balance = balance - amount;
           System.out.println("Withdrawal of $" + amount + "cannot be completed. Your balance is $" + balance);
       } else {
           System.out.println("Your balance has changed by $" + amount + "and now it is: $" + balance);
       }
    }
    public int getBalance() {
        return balance;
    }
}
