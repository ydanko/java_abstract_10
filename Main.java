public class Main {
    public static void main(String[] args) {
        BankAccount amount = new BankAccount();
        Deposit deposit = new Deposit();

        deposit.setBalance(500);
        deposit.setBalance(150);
        deposit.setBalance(35);

        deposit.setBalance(-40);
        deposit.setBalance(-120);
        deposit.setBalance(-900);

        System.out.println("Your current balance is $" + deposit.getBalance());

    }
}

